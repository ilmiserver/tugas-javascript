//SOAL 1
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

var compare = function(a, b) {
    return parseInt(a) - parseInt(b);
  }
  
    console.log(daftarBuah.sort(compare));

for (var i=0; i < daftarBuah.length; i++) {
  for (var j = 0; j<i; j++) {
    if (parseInt(daftarBuah[i])<parseInt(daftarBuah[j])) {
      var x = daftarBuah[i];
      daftarBuah[i] = daftarBuah[j];
      daftarBuah[j] = x;
    }
  }
}

console.log(daftarBuah);

//SOAL 2
var kalimat = "saya sangat senang belajar javascript";
var kata = kalimat.split(" ");
console.log(kata);

//SOAL 3
var buah = [
    {
      'nama' : 'strawberry',
      'warna': 'merah',
      'ada bijinya': 'tidak',
      'harga': 9000,
    },
    {
      'nama' : 'jeruk',
      'warna': 'oranye',
      'ada bijinya': 'ada',
      'harga': 8000,
    },
    {
      'nama' : 'semangka',
      'warna': 'hijau & merah',
      'ada bijinya': 'ada',
      'harga': 10000,
    },
    {
      'nama' : 'pisang',
      'warna': 'kuning',
      'ada bijinya': 'tidak',
      'harga': 5000,
    }
];

console.log(buah[0]);
